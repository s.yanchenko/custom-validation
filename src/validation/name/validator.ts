import { generateMaxException, INVALID_TYPE_ERROR_MESSAGE } from "@/exceptions";

export const nameValidator = (data: { state: string }) => (value: unknown) => {
  if (typeof value !== "string") {
    return { message: INVALID_TYPE_ERROR_MESSAGE };
  }

  if (data.state === "draft" && value?.length > 8) {
    return { message: generateMaxException(8) };
  }
  return undefined;
};
