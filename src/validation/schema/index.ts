import { z } from "zod";
import { FORM_FIELDS } from "@/constants/form";
import { generateMinException, RULE_DEFAULT_ERROR } from "@/exceptions";

export const zodSchema = z.object({
  [FORM_FIELDS.NAME]: z
    .string(RULE_DEFAULT_ERROR)
    .min(5, { message: generateMinException(5) }),
});
