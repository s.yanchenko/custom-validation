export const REQUIRED_ERROR_MESSAGE = "Обязательное поле";
export const INVALID_TYPE_ERROR_MESSAGE = "Введены некорректные данные";

export const RULE_DEFAULT_ERROR = {
  required_error: REQUIRED_ERROR_MESSAGE,
  invalid_type_error: INVALID_TYPE_ERROR_MESSAGE,
};
export const generateMinException = (count: number) =>
  `Значение поля должно быть минимум ${count} символов`;

export const generateMaxException = (count: number) =>
  `Значение поля должно быть максимум ${count} символов`;
