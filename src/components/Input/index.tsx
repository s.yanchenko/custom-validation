import {
  FieldValues,
  Path,
  RegisterOptions,
  useController,
  useFormContext,
} from "react-hook-form";
import { InputHTMLAttributes } from "react";
import { useValidator } from "@/hooks/useValidator.tsx";

type InputProps<
  TFieldValues extends FieldValues = FieldValues,
  TName extends Path<TFieldValues> = Path<TFieldValues>,
> = InputHTMLAttributes<HTMLInputElement> & {
  name: TName;
  validator?: (value: unknown) => { message: string } | undefined;
  rules?:
    | Omit<
        RegisterOptions<TFieldValues, Path<TFieldValues>>,
        "valueAsNumber" | "valueAsDate" | "setValueAs" | "disabled"
      >
    | undefined;
};

function Input<
  TFieldValues extends FieldValues = FieldValues,
  TName extends Path<TFieldValues> = Path<TFieldValues>,
>(props: InputProps<TFieldValues, TName>) {
  const { name, validator, rules, ...rest } = props;

  const { control } = useFormContext<TFieldValues>();
  const {
    field,
    fieldState: { error },
  } = useController<TFieldValues>({ control, name, rules });

  const defaultValue = String();

  console.log("Render Input");

  const { handleValidateValue } = useValidator<TFieldValues, TName>({
    name,
    value: field.value,
    validator,
  });

  return (
    <div>
      <input
        {...rest}
        name={field.name}
        value={field.value || defaultValue}
        onChange={field.onChange}
        onBlur={handleValidateValue}
      />
      {error && <span style={{ color: "red" }}>{error.message}</span>}
    </div>
  );
}

export default Input;
