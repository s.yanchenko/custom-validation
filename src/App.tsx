import { FormProvider, useForm } from "react-hook-form";
import { FormFields } from "@/types/form";
import Input from "@/components/Input";
import { FORM_FIELDS } from "@/constants/form";
import { nameValidator } from "@/validation/name/validator.ts";
import { zodResolver } from "@hookform/resolvers/zod";
import { zodSchema } from "@/validation/schema";

function App() {
  const methods = useForm<FormFields>({
    mode: "onBlur",
    reValidateMode: "onBlur",
    resolver: zodResolver(zodSchema),
  });
  console.log("isValid", methods.formState.isValid);
  console.log("errors", methods.formState.errors);

  const state = "draft";

  return (
    <FormProvider {...methods}>
      <Input name={FORM_FIELDS.NAME} validator={nameValidator({ state })} />
      <button
        disabled={!methods.formState.isValid}
        onClick={() => console.log("Submit")}
      >
        Submit
      </button>
    </FormProvider>
  );
}

export default App;
