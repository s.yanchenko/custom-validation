import { FieldValues, Path, useFormContext } from "react-hook-form";
import { useCallback, useEffect } from "react";

export const useValidator = <
  TFieldValues extends FieldValues = FieldValues,
  TName extends Path<TFieldValues> = Path<TFieldValues>,
>({
  name,
  value,
  validator,
}: {
  name: TName;
  value: unknown;
  validator?: (value: unknown) => { message: string } | undefined;
}) => {
  const { setError, clearErrors } = useFormContext<TFieldValues>();

  useEffect(() => {
    validator && handleValidateValue();
  }, []);

  const handleValidateValue = useCallback(() => {
    if (!validator) {
      return;
    }

    const result = validator?.(value);
    if (result) {
      console.log("setError");
      setError(name, result);
    } else {
      console.log("clearErrors");
      clearErrors([name]);
    }
  }, [clearErrors, name, setError, validator, value]);

  return { handleValidateValue };
};
